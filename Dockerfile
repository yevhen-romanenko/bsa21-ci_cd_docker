FROM node:10 AS react_build

WORKDIR /app

COPY . .

RUN npm run build

FROM nginx:alpine

WORKDIR /usr/share/nginx/html

# COPY --from=react_build /app/build .

COPY --from=react_build /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

EXPOSE 80 
CMD ["nginx","-g","daemon off;"]

